﻿namespace lab5_paintsharp {
    partial class formMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.pbPlatno = new System.Windows.Forms.PictureBox();
            this.gbKistovi = new System.Windows.Forms.GroupBox();
            this.rbKvadrat = new System.Windows.Forms.RadioButton();
            this.rbKruznica = new System.Windows.Forms.RadioButton();
            this.rbTocka = new System.Windows.Forms.RadioButton();
            this.gbPostavkeKista = new System.Windows.Forms.GroupBox();
            this.pnlColor = new System.Windows.Forms.Panel();
            this.rbKist3px = new System.Windows.Forms.RadioButton();
            this.rbKist2px = new System.Windows.Forms.RadioButton();
            this.rbKist1px = new System.Windows.Forms.RadioButton();
            this.gbUndoRedo = new System.Windows.Forms.GroupBox();
            this.btnRedo = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.lbPovijest = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlatno)).BeginInit();
            this.gbKistovi.SuspendLayout();
            this.gbPostavkeKista.SuspendLayout();
            this.gbUndoRedo.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbPlatno
            // 
            this.pbPlatno.BackColor = System.Drawing.Color.White;
            this.pbPlatno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbPlatno.Location = new System.Drawing.Point(12, 12);
            this.pbPlatno.Name = "pbPlatno";
            this.pbPlatno.Size = new System.Drawing.Size(543, 374);
            this.pbPlatno.TabIndex = 0;
            this.pbPlatno.TabStop = false;
            this.pbPlatno.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbPlatno_MouseDown);
            // 
            // gbKistovi
            // 
            this.gbKistovi.Controls.Add(this.rbKvadrat);
            this.gbKistovi.Controls.Add(this.rbKruznica);
            this.gbKistovi.Controls.Add(this.rbTocka);
            this.gbKistovi.Location = new System.Drawing.Point(12, 393);
            this.gbKistovi.Name = "gbKistovi";
            this.gbKistovi.Size = new System.Drawing.Size(185, 65);
            this.gbKistovi.TabIndex = 1;
            this.gbKistovi.TabStop = false;
            this.gbKistovi.Text = "Kistovi";
            // 
            // rbKvadrat
            // 
            this.rbKvadrat.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbKvadrat.AutoSize = true;
            this.rbKvadrat.Location = new System.Drawing.Point(124, 28);
            this.rbKvadrat.Name = "rbKvadrat";
            this.rbKvadrat.Size = new System.Drawing.Size(54, 23);
            this.rbKvadrat.TabIndex = 2;
            this.rbKvadrat.TabStop = true;
            this.rbKvadrat.Text = "Kvadrat";
            this.rbKvadrat.UseVisualStyleBackColor = true;
            // 
            // rbKruznica
            // 
            this.rbKruznica.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbKruznica.AutoSize = true;
            this.rbKruznica.Location = new System.Drawing.Point(60, 28);
            this.rbKruznica.Name = "rbKruznica";
            this.rbKruznica.Size = new System.Drawing.Size(58, 23);
            this.rbKruznica.TabIndex = 1;
            this.rbKruznica.TabStop = true;
            this.rbKruznica.Text = "Kružnica";
            this.rbKruznica.UseVisualStyleBackColor = true;
            // 
            // rbTocka
            // 
            this.rbTocka.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbTocka.AutoSize = true;
            this.rbTocka.Checked = true;
            this.rbTocka.Location = new System.Drawing.Point(6, 28);
            this.rbTocka.Name = "rbTocka";
            this.rbTocka.Size = new System.Drawing.Size(48, 23);
            this.rbTocka.TabIndex = 0;
            this.rbTocka.TabStop = true;
            this.rbTocka.Text = "Točka";
            this.rbTocka.UseVisualStyleBackColor = true;
            // 
            // gbPostavkeKista
            // 
            this.gbPostavkeKista.Controls.Add(this.pnlColor);
            this.gbPostavkeKista.Controls.Add(this.rbKist3px);
            this.gbPostavkeKista.Controls.Add(this.rbKist2px);
            this.gbPostavkeKista.Controls.Add(this.rbKist1px);
            this.gbPostavkeKista.Location = new System.Drawing.Point(203, 393);
            this.gbPostavkeKista.Name = "gbPostavkeKista";
            this.gbPostavkeKista.Size = new System.Drawing.Size(166, 65);
            this.gbPostavkeKista.TabIndex = 2;
            this.gbPostavkeKista.TabStop = false;
            this.gbPostavkeKista.Text = "Postavke kista";
            // 
            // pnlColor
            // 
            this.pnlColor.BackColor = System.Drawing.Color.Black;
            this.pnlColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlColor.Location = new System.Drawing.Point(128, 30);
            this.pnlColor.Name = "pnlColor";
            this.pnlColor.Size = new System.Drawing.Size(28, 20);
            this.pnlColor.TabIndex = 4;
            // 
            // rbKist3px
            // 
            this.rbKist3px.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbKist3px.AutoSize = true;
            this.rbKist3px.Location = new System.Drawing.Point(86, 28);
            this.rbKist3px.Name = "rbKist3px";
            this.rbKist3px.Size = new System.Drawing.Size(34, 23);
            this.rbKist3px.TabIndex = 3;
            this.rbKist3px.TabStop = true;
            this.rbKist3px.Text = "3px";
            this.rbKist3px.UseVisualStyleBackColor = true;
            // 
            // rbKist2px
            // 
            this.rbKist2px.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbKist2px.AutoSize = true;
            this.rbKist2px.Location = new System.Drawing.Point(46, 28);
            this.rbKist2px.Name = "rbKist2px";
            this.rbKist2px.Size = new System.Drawing.Size(34, 23);
            this.rbKist2px.TabIndex = 2;
            this.rbKist2px.TabStop = true;
            this.rbKist2px.Text = "2px";
            this.rbKist2px.UseVisualStyleBackColor = true;
            // 
            // rbKist1px
            // 
            this.rbKist1px.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbKist1px.AutoSize = true;
            this.rbKist1px.Checked = true;
            this.rbKist1px.Location = new System.Drawing.Point(6, 28);
            this.rbKist1px.Name = "rbKist1px";
            this.rbKist1px.Size = new System.Drawing.Size(34, 23);
            this.rbKist1px.TabIndex = 1;
            this.rbKist1px.TabStop = true;
            this.rbKist1px.Text = "1px";
            this.rbKist1px.UseVisualStyleBackColor = true;
            // 
            // gbUndoRedo
            // 
            this.gbUndoRedo.Controls.Add(this.btnRedo);
            this.gbUndoRedo.Controls.Add(this.btnUndo);
            this.gbUndoRedo.Location = new System.Drawing.Point(375, 395);
            this.gbUndoRedo.Name = "gbUndoRedo";
            this.gbUndoRedo.Size = new System.Drawing.Size(180, 63);
            this.gbUndoRedo.TabIndex = 3;
            this.gbUndoRedo.TabStop = false;
            // 
            // btnRedo
            // 
            this.btnRedo.Location = new System.Drawing.Point(95, 25);
            this.btnRedo.Name = "btnRedo";
            this.btnRedo.Size = new System.Drawing.Size(75, 23);
            this.btnRedo.TabIndex = 1;
            this.btnRedo.Text = "Redo";
            this.btnRedo.UseVisualStyleBackColor = true;
            // 
            // btnUndo
            // 
            this.btnUndo.Location = new System.Drawing.Point(14, 25);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(75, 23);
            this.btnUndo.TabIndex = 0;
            this.btnUndo.Text = "Undo";
            this.btnUndo.UseVisualStyleBackColor = true;
            // 
            // lbPovijest
            // 
            this.lbPovijest.FormattingEnabled = true;
            this.lbPovijest.Location = new System.Drawing.Point(562, 12);
            this.lbPovijest.Name = "lbPovijest";
            this.lbPovijest.Size = new System.Drawing.Size(271, 446);
            this.lbPovijest.TabIndex = 4;
            // 
            // formMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 470);
            this.Controls.Add(this.lbPovijest);
            this.Controls.Add(this.gbUndoRedo);
            this.Controls.Add(this.gbPostavkeKista);
            this.Controls.Add(this.gbKistovi);
            this.Controls.Add(this.pbPlatno);
            this.Name = "formMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PaintSharp";
            this.Load += new System.EventHandler(this.formMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbPlatno)).EndInit();
            this.gbKistovi.ResumeLayout(false);
            this.gbKistovi.PerformLayout();
            this.gbPostavkeKista.ResumeLayout(false);
            this.gbPostavkeKista.PerformLayout();
            this.gbUndoRedo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbPlatno;
        private System.Windows.Forms.GroupBox gbKistovi;
        private System.Windows.Forms.RadioButton rbKvadrat;
        private System.Windows.Forms.RadioButton rbKruznica;
        private System.Windows.Forms.RadioButton rbTocka;
        private System.Windows.Forms.GroupBox gbPostavkeKista;
        private System.Windows.Forms.Panel pnlColor;
        private System.Windows.Forms.RadioButton rbKist3px;
        private System.Windows.Forms.RadioButton rbKist2px;
        private System.Windows.Forms.RadioButton rbKist1px;
        private System.Windows.Forms.GroupBox gbUndoRedo;
        private System.Windows.Forms.Button btnRedo;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.ListBox lbPovijest;
    }
}

