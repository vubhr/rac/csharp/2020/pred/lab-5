﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab5_paintsharp {
    public partial class formMain : Form {
        Engine engine;

        public formMain() {
            InitializeComponent();
        }

        private void formMain_Load(object sender, EventArgs e) {
            engine = new Engine(pbPlatno.Width, pbPlatno.Height);
        }

        private void pbPlatno_MouseDown(object sender, MouseEventArgs e) {
            engine.NacrtajTocku(e.X, e.Y);
            OsvjeziPlatno();
        }

        private void OsvjeziPlatno() {
            pbPlatno.Image = engine.Platno;
            lbPovijest.Items.Clear();
            lbPovijest.Items.AddRange(engine.Povijest.ToArray());
        }
    }
}
