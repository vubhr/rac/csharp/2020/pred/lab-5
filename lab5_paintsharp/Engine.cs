﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5_paintsharp {
    class Engine {
        public Engine(int sirina, int visina) {
            Platno = new Bitmap(sirina, visina);
            PlatnoSirina = sirina;
            PlatnoVisina = visina;
            Povijest = new List<string>();
        }

        public void NacrtajTocku(int x, int y) {
            Graphics g = Graphics.FromImage(Platno);
            g.DrawEllipse(new Pen(Brushes.Black), x, y, 2, 2);
            Povijest.Add(string.Format("Dodana tocka na {0},{1}", x, y));
        }

        public void NacrtajKvadrat(int x, int y) {
            Graphics g = Graphics.FromImage(Platno);
        }

        public List<string> Povijest { get; set; }
        public Bitmap Platno { get; set; }
        public int PlatnoSirina { get; set; }
        public int PlatnoVisina { get; set; }
    }
}
